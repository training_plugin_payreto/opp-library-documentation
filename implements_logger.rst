Implements Logger
#################

This guide will explain how to implements Logger in this library. However, it is OK if you not follow this guide since the library itself has built-in logger implementation (``Payreto\Opp\Logger``).

First thing you must do is create a class that implements ``Payreto\Opp\Interfaces\Logger`` interface. Here is an example class from ``Payreto\Opp\Logger``:

.. code-block:: php

    require_once __DIR__.'/Interfaces/Logger.php';

    use Payreto\Opp\Interfaces\Logger as LoggerContract;

    class Logger implements LoggerContract
    {
        /**
         * The log file location.
         *
         * @var string
         */
        protected $file;

        /**
         * Create a new Logger instance.
         *
         * @param  string|null  $file
         * @return void
         */
        public function __construct($file = null)
        {
            $this->file = $file ?: __DIR__.'/../logs/log.txt';

            $this->createFileIfNotExists();
        }

        /**
         * Log the given arguments.
         *
         * @param  string  $level
         * @param  string  $message
         * @param  array  $context
         * @return void
         */
        public function write($level, $message, $context = array())
        {
            $context = print_r($context, true);

            $message = sprintf(
                '[%s][%s] %s : %s%s', date('Y-m-d H:i:s'), $level, $message, $context, PHP_EOL
            );

            error_log($message, 3, $this->file);
        }

        /**
         * Create the log file if not exists.
         *
         * @return void
         */
        protected function createFileIfNotExists()
        {
            if (! file_exists($this->file)) {
                $fileHandler = fopen($this->file, 'w');

                fclose($fileHandler);
            }
        }
    }

Then, every time you instantiate ``Payreto\Opp\Opp``, don\'t forget to set the logger implementation using ``Payreto\Opp\Opp::setLogger`` method:

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';
    require_once '/path/to/your/logger_implementation.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $opp->setLogger(new LoggerImplementation($args));
