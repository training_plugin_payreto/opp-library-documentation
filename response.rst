Response
########

The HTTP JSON response that we get from OPP always be wrapped into ``Payreto\Opp\Http\Response`` object that has some convenient methods.

.. php:namespace:: Payreto\Opp\Http

.. php:class:: Response

    .. php:method:: get($key)

        Get a data by given "dot" notation key.

        :param string $key: The "dot" notation key
        :returns: ``mixed``

        Example:

        .. code-block:: php

            // Assume that we already have the Response object which wrap a JSON like this:
            // {"result":{"code":"000.200.100","description":"successfully created checkout"}}
            // To get the "description", just use "result.description" key:

            $descriptionResult = $response->get('result.description');
            // Returns: "successfully created checkout"

    .. php:method:: isPending()

        Determine if the payment is pending.

        :returns: ``bool``

        Example:

        .. code-block:: php

            // Assume that we already have the Response object which wrap a JSON like this:
            // {"result":{"code":"000.200.100","description":"successfully created checkout"}}
            // To get the "description", just use "result.description" key:

            if ($response->isPending()) {
                // handle pending payment here...
            }

    .. php:method:: isRejected()

        Determine if the payment is rejected.

        :returns: ``bool``

        Example:

        .. code-block:: php

            // Assume that we already have the Response object which wrap a JSON like this:
            // {"result":{"code":"000.200.100","description":"successfully created checkout"}}
            // To get the "description", just use "result.description" key:

            if ($response->isRejected()) {
                // handle rejected payment here...
            }

    .. php:method:: isSuccessful()

        Determine if the payment is successful.

        :returns: ``bool``

        Example:

        .. code-block:: php

            // Assume that we already have the Response object which wrap a JSON like this:
            // {"result":{"code":"000.200.100","description":"successfully created checkout"}}
            // To get the "description", just use "result.description" key:

            if ($response->isSuccessful()) {
                // handle successful payment here...
            }

    .. php:method:: shouldBeReviewed()

        Determine if the payment should be reviewed.

        :returns: ``bool``

        Example:

        .. code-block:: php

            // Assume that we already have the Response object which wrap a JSON like this:
            // {"result":{"code":"000.200.100","description":"successfully created checkout"}}
            // To get the "description", just use "result.description" key:

            if ($response->shouldBeReviewed()) {
                // handle payment that should manually reviewed here...
            }

    .. php:method:: toArray()

        Returns the array representation of the response.

        :returns: ``array``

        Example:

        .. code-block:: php

            // Assume that we already have the Response object which wrap a JSON like this:
            // {"result":{"code":"000.200.100","description":"successfully created checkout"}}
            // To get the "description", just use "result.description" key:

            $responseArray = $response->toArray();
            // $responseArray = array(
            //     'result' => array(
            //         'code' => '000.200.100',
            //         'description' => 'successfully created checkout',
            //     ),
            // );

            echo $responseArray['result']['code'];
            // print "000.200.100"

    .. php:method:: toJson()

        Returns the JSON response.

        :returns: ``string``

        Example:

        .. code-block:: php

            // Assume that we already have the Response object which wrap a JSON like this:
            // {"result":{"code":"000.200.100","description":"successfully created checkout"}}
            // To get the "description", just use "result.description" key:

            $responseJson = $response->toJson();
            // $responseJson = '{"result":{"code":"000.200.100","description":"successfully created checkout"}}';

            echo $responseJson;
            // print '{"result":{"code":"000.200.100","description":"successfully created checkout"}}'
