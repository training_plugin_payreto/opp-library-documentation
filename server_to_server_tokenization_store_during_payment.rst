Server-to-Server: Tokenization: Store During Payment
####################################################

Here, we just perform a basic synchronous payment with additional parameter: ``'createRegistration' => 'true'``. But this time, we will get the ``registrationId`` in the response. Example:

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $paymentBrandRequiredParameters = array(
        'paymentType' => 'DB',
        'amount' => '92.00',
        'currency' => 'EUR',
        'paymentBrand' => 'VISA',
        'card.number' => '4200000000000000',
        'card.holder' => 'Jane Jones',
        'card.expiryMonth' => '05',
        'card.expiryYear' => '2018',
        'card.cvv' => '123',
        'createRegistration' => 'true',
    ); // This is just example for VISA, you must follow the Payment Brand's required parameters.

    $result = $opp->syncServerToServer()->pay($paymentBrandRequiredParameters);

    // $result = A Response object that wrap: '{"id":"8a8294495d2c4bcd015d3146abf3239b","registrationId":"8a82944a5d2c3545015d3146aac62b53","paymentType":"DB","paymentBrand":"VISA","amount":"92.00","currency":"EUR","descriptor":"1338.7672.4386 OPP_Channel ","result":{"code":"000.100.110","description":"Request successfully processed in 'Merchant in Integrator Test Mode'"},"resultDetails":{"clearingInstituteName":"Elavon-euroconex_UK_Test"},"card":{"bin":"420000","last4Digits":"0000","holder":"Jane Jones","expiryMonth":"05","expiryYear":"2018"},"customParameters":{"OPP_card.bin":"420000"},"risk":{"score":"100"},"buildNumber":"977894f754b0f715838835a65fdbfee69a2e808d@2017-07-07 08:04:06 +0000","timestamp":"2017-07-11 10:51:41+0000","ndc":"8a8294174b7ecb28014b9699220015ca_edad752a84c4457d89df26f0a2975a04"}';
