Transaction Report
##################

There are currently 2 ways to get a report with the details of an existing payment:

* Transaction search using ``paymentId``
* Transaction search using ``merchantTransactionId``

Transaction search using ``paymentId``
**************************************

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $paymentId = '8a82944a4cc25ebf014cc2c782423202';

    $result = $opp->report()->searchByPaymentId($paymentId);

    // $result = A Response object that wrap: '{"id":"8a82944a4cc25ebf014cc2c782423202","paymentBrand":"AMEX","result":{"code":"000.100.110","description":"Request successfully processed in 'Merchant in Integrator Test Mode'"},"card":{"bin":"******","last4Digits":"7770","holder":"Jane Jones","expiryMonth":"05","expiryYear":"2018"},"buildNumber":"560da3ae953c63575278ebc7c23ba0202a0e0223@2017-07-18 11:11:04 +0000","timestamp":"2015-04-16 15:09:57+0000","ndc":"8a8294174b7ecb28014b9699220015ca_5efea8962dcd46aba155bfcb9cb6b94a"}';

Transaction search using ``merchantTransactionId``
**************************************************

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $merchantTransactionId = 'test123';

    $result = $opp->report()->searchByMerchantTransactionId($merchantTransactionId);

    // $result = A Response object that wrap: '{"result":{"code":"000.000.100","description":"successful request"},"buildNumber":"560da3ae953c63575278ebc7c23ba0202a0e0223@2017-07-18 11:11:04 +0000","timestamp":"2017-07-20 09:11:55+0000","ndc":"8a8294174b7ecb28014b9699220015ca_f33b0c9447304217a5ea55ad962cb86f","payments":[{"id":"8a82944956279c9401562c565fb34fe4","paymentType":"DB","paymentBrand":"VISA","amount":"92.00","currency":"EUR","descriptor":"8730.0486.0066 OPP_Channel","merchantTransactionId":"test123","result":{"code":"000.100.110","description":"Request successfully processed in 'Merchant in Integrator Test Mode'"},"card":{"bin":"420000","last4Digits":"0000","holder":"Jane Jones","expiryMonth":"05","expiryYear":"2018"},"threeDSecure":{"eci":"07"},"risk":{"score":"0"},"timestamp":"2016-07-27 12:31:13+0000"}]}';
