COPYandPAY: Tokenization: Store As Stand-Alone: Prepare the checkout
####################################################################

A registration-only transaction with COPYandPAY is basically using the same workflow and parameters as a payment:

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $parameters = array(
        'amount' => '92.00',
        'currency' => 'EUR',
    );

    $result = $opp->copyAndPay()->prepareCheckoutWithTokenization($parameters, true);

    // $result = A Response object that wrap: '{"result":{"code":"000.200.100","description":"successfully created checkout"},"buildNumber":"977894f754b0f715838835a65fdbfee69a2e808d@2017-07-07 08:04:06 +0000","timestamp":"2017-07-11 09:29:06+0000","ndc":"9AF600DEA964E008F1345CDFB8B5EC85.sbg-vm-tx01","id":"9AF600DEA964E008F1345CDFB8B5EC85.sbg-vm-tx01"}';
