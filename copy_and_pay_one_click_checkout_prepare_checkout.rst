COPYandPAY: One-Click Checkout: Prepare the checkout
####################################################

First perform a server-to-server POST request to prepare the checkout, this should include the registration IDs of the customer as shown below.

The registration IDs should be sent in the registrations[n].id parameter, where n is a sequence number from zero, incrementing for each of the customer's registration IDs. For example, if the customer has two accounts on file, you would send registrations[0].id = {first registration.id} and registrations[1].id = {second registration.id}.

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $parameters = array(
        'amount' => '92.00',
        'currency' => 'EUR',
        'paymentType' => 'DB',
        'registrations[0].id' => '8a8294495d18c9b2015d1bbad95a6fcb',
    );

    $result = $opp->copyAndPay()->oneClickCheckout($parameters);

    // $result = A Response object that wrap: '{"result":{"code":"000.200.100","description":"successfully created checkout"},"buildNumber":"977894f754b0f715838835a65fdbfee69a2e808d@2017-07-07 08:04:06 +0000","timestamp":"2017-07-11 09:48:57+0000","ndc":"09058D8895BE409424BB1C3A0BCBE8B7.sbg-vm-tx01","id":"09058D8895BE409424BB1C3A0BCBE8B7.sbg-vm-tx01"}';
