COPYandPAY: Tokenization: Store As Stand-Alone: Get Registration Result
#######################################################################

You can get the registration result by using ``checkoutId`` from previous step:

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $checkoutId = '8a82944a4cc25ebf014cc2c782423202';

    $result = $opp->copyAndPay()->getRegistrationResult($checkoutId);

    // $result = A Response object that wrap: '{"id":"8a82944a5d54fdd4015d5a34b02039c4","paymentBrand":"VISA","result":{"code":"000.100.110","description":"Request successfully processed in 'Merchant in Integrator Test Mode'"},"card":{"bin":"411111","last4Digits":"1111","holder":"roni","expiryMonth":"06","expiryYear":"2021"},"risk":{"score":"0"},"buildNumber":"560da3ae953c63575278ebc7c23ba0202a0e0223@2017-07-18 11:11:04 +0000","timestamp":"2017-07-19 09:36:28+0000","ndc":"9C2A9BAEA3E7BB121108829CE5D4B515.sbg-vm-tx01"}';
