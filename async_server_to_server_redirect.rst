Asynchronous Server-to-Server: Redirect the shopper
###################################################

Next, redirect the shopper to the url that you must parse from the response in previous step (``redirect.url`` and ``redirect.parameters``).

Example:

.. code-block:: php

    // $result = A Response object that wrap: '{"id":"8a82944a5d2c3545015d3133138902f7","paymentType":"PA","paymentBrand":"PAYPAL","amount":"92.12","currency":"EUR","descriptor":"6902.2598.4162 Paypal_Channel ","result":{"code":"000.200.000","description":"transaction pending"},"resultDetails":{"ConnectorTxID3":"2c3545015d3133138902f7","ConnectorTxID2":"8a82944a","AcquirerResponse":"Success","ConnectorTxID1":"8a82944a5d2c3545015d3133138902f7","clearingInstituteName":"PayPal Test"},"redirect":{"url":"https://test.ppipe.net/connectors/simulator;jsessionid=3536E151BB214EC4364C871C09F716A4.sbg-vm-con02","parameters":[{"name":"ndcid","value":"8a8294174b7ecb28014b9699220015ca_2777ec10a7264efa90d5e958e6ae014c"},{"name":"useraction","value":"commit"},{"name":"cmd","value":"_express-checkout"},{"name":"token","value":"EC-4MJ162715G749641T"},{"name":"connector","value":"PAYPAL"}]},"buildNumber":"977894f754b0f715838835a65fdbfee69a2e808d@2017-07-07 08:04:06 +0000","timestamp":"2017-07-11 10:30:17+0000","ndc":"8a8294174b7ecb28014b9699220015ca_2777ec10a7264efa90d5e958e6ae014c"}';

    $url = $result->get('redirect.url');

    $query = array();

    foreach ($result->get('redirect.parameters') as $parameter) {
        $query[$parameter['name']] = $parameter['value'];
    }

    header('Location: '.$url.'?'.http_build_query($query));
    // Will be redirected to: https://test.ppipe.net/connectors/simulator;jsessionid=3536E151BB214EC4364C871C09F716A4.sbg-vm-con02?ndcid=8a8294174b7ecb28014b9699220015ca_2777ec10a7264efa90d5e958e6ae014c&useraction=commit&cmd=_express-checkout&token=EC-4MJ162715G749641T&connector=PAYPAL
