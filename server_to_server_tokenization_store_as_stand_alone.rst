Server-to-Server: Tokenization: Store As Stand Alone
####################################################

Here, we can register the shopper independently from payment. We will directly receive a registration object in the response:

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $paymentBrandRequiredParameters = array(
        'paymentBrand' => 'VISA',
        'card.number' => '4200000000000000',
        'card.holder' => 'Jane Jones',
        'card.expiryMonth' => '05',
        'card.expiryYear' => '2018',
        'card.cvv' => '123',
    ); // This is just example for VISA, you must follow the Payment Brand's required parameters.

    $result = $opp->syncServerToServer()->generateRegistrationId($paymentBrandRequiredParameters);

    // $result = A Response object that wrap: '{"id":"8a82944a5d2c3545015d3151550b3583","result":{"code":"000.100.110","description":"Request successfully processed in 'Merchant in Integrator Test Mode'"},"card":{"bin":"420000","last4Digits":"0000","holder":"Jane Jones","expiryMonth":"05","expiryYear":"2018"},"risk":{"score":"0"},"buildNumber":"977894f754b0f715838835a65fdbfee69a2e808d@2017-07-07 08:04:06 +0000","timestamp":"2017-07-11 11:03:20+0000","ndc":"8a8294174d0a8edd014d0a9f97430077_8179e043fd404b37a537551b4a372a69"}';
