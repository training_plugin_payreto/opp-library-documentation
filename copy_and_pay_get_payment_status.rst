COPYandPAY: Get the payment status
##################################

After the payment has been processed and your customer redirected to ``shopperResultUrl``, you can get the payment status by given ``checkoutId`` like this following code:

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $checkoutId = '02F3CC224E1D7270475D7F07033B68CD.sbg-vm-tx02';

    $result = $opp->copyAndPay()->getPaymentStatus($checkoutId);

    // $result = A Response object that wrap: '{"id":"8a82944a5d2c3545015d30e0748a62fe","paymentType":"DB","paymentBrand":"VISA","amount":"92.00","currency":"EUR","descriptor":"6328.4790.5442 OPP_Channel","result":{"code":"000.100.110","description":"Request successfully processed in 'Merchant in Integrator Test Mode'"},"resultDetails":{"clearingInstituteName":"Elavon-euroconex_UK_Test"},"card":{"bin":"411111","last4Digits":"1111","holder":"roni","expiryMonth":"06","expiryYear":"2021"},"threeDSecure":{"eci":"05","verificationId":"AAACAgSRBklmQCFgMpEGAAAAAAA=","xid":"CAACCVVUlwCXUyhQNlSXAAAAAAA="},"customParameters":{"CTPE_DESCRIPTOR_TEMPLATE":""},"risk":{"score":"100"},"buildNumber":"977894f754b0f715838835a65fdbfee69a2e808d@2017-07-07 08:04:06 +0000","timestamp":"2017-07-11 09:00:11+0000","ndc":"02F3CC224E1D7270475D7F07033B68CD.sbg-vm-tx02"}';
