GIROPAY
#######

Giropay support COPYandPAY and Server-to-Server. Let's see how to integrate those both features!

COPYandPAY
**********

COPYandPAY is JavaScript payment widget that sends sensitive payment data directly from the shopper's browser to the Open Payment Platform (OPP).

To implement COPYandPAY, Giropay needs this following parameters:

* ``amount``
* ``currency``
* ``paymentType``

There are just three simple steps required to integrate:

1. :doc:`Prepare the checkout <copy_and_pay_prepare_checkout>`
2. :doc:`Create the payment form <copy_and_pay_create_payment_form>`
3. :doc:`Get the payment status <copy_and_pay_get_payment_status>`

Tokenization
============

Tokenization allows you to store payment data for later use. This can be useful for recurring and/or one-click payment scenarios.

This guide will describe how to store account details using COPYandPAY. COPYandPAY provides two options for storing the payment data:

* **Store the data during a payment**: When a shopper is checking out for the first time, he has to fill in his complete payment- and address data. Use this option to automatically store his data during the payment for reuse in later transactions.

* **Store the data as stand-alone**: If your site provides shoppers with an administrative area where they can register their payment details independent of a checkout-process, this option is for you.

Store the data during a payment
-------------------------------

You have two options for achieving this in OPP: Merchant-determined tokenization and Shopper-determined tokenization. But this library only cover Merchant-determined tokenization.

1. :doc:`Prepare the checkout <copy_and_pay_tokenization_store_during_payment_merchant_prepare_checkout>`
2. Create the payment form like :doc:`step 2 of the COPYandPAY checkout  <copy_and_pay_create_payment_form>`
3. After the shopper redirected back to your ``shopperResultUrl``, we can retrieve the response information as usual (:doc:`step 3 <copy_and_pay_get_payment_status>`) but this time the response will include a ``registrationId`` (token) and useful card information that you can store for future 'one-click payment' requests.

Store the data as stand-alone
-----------------------------

With COPYandPAY it is also possible to create a just registration separate from any later payment.

1. :doc:`Prepare the checkout <copy_and_pay_tokenization_store_as_stand_alone_prepare_checkout>`
2. Same with :doc:`step 2 <copy_and_pay_create_payment_form>`, COPYandPAY will render the payment form as usual. But send the form to ``https://{test.}oppwa.com/v1/checkouts/{checkoutId}/registration``
3. :doc:`Get registration result <copy_and_pay_tokenization_store_as_stand_alone_get_registration_result>`

One-Click Checkout
==================

One-Click Checkout allows you to achieve a significant speedup of the checkout process by re-using the data a customer entered for a previous transaction/registration.

1. :doc:`Prepare Checkout <copy_and_pay_one_click_checkout_prepare_checkout>`
2. :doc:`Create the payment form <copy_and_pay_create_payment_form>`. When COPYandPAY builds up the payment form it automatically fetches the registrations data from the server and displays the pre-filled widgets to the shopper
3. :doc:`Get the payment status <copy_and_pay_get_payment_status>`

Recurring
=========

1. :doc:`Prepare Checkout <copy_and_pay_recurring_prepare_checkout>`
2. :doc:`Create the payment form <copy_and_pay_create_payment_form>`
3. :doc:`Get the payment status <copy_and_pay_get_payment_status>`

Server-to-Server
****************

Server-to-Server API allows payment acceptance services to be integrated directly, offering fully flexible workflows for frontend and backend processing.

Giropay using **Asynchronous Workflow** where a redirection takes place to allow the account holder to complete/verify the payment. After this the account holder is redirected back to the ``shopperResultUrl`` and the status of the payment can be queried.

To implement Server-to-Server, Giropay needs this following parameters:

* ``amount``
* ``currency``
* ``paymentType``
* ``paymentBrand`` (with value ``'GIROPAY'``)
* ``bankAccount.bic``
* ``bankAccount.iban``
* ``bankAccount.country``
* ``shopperResultUrl``

There are three steps to integrate Server-to-Server:

1. :doc:`Initial Payment <async_server_to_server_initial_payment>`

2. :doc:`Redirect the shopper <async_server_to_server_redirect>`

3. :doc:`Get Payment Status <async_server_to_server_payment_status>`
