COPYandPAY: Tokenization: Store During Payment: Merchant-determined tokenization: Prepare the checkout
######################################################################################################

During the checkout process you can store the data by adding an additional parameter (``'createRegistration' => 'true'``) to the normal prepare checkout request as described in :doc:`step 1 of the COPYandPAY checkout <copy_and_pay_prepare_checkout>`:

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $parameters = array(
        'amount' => '92.00',
        'currency' => 'EUR',
        'paymentType' => 'DB',
        'createRegistration' => 'true',
    );

    $result = $opp->copyAndPay()->prepareCheckout($parameters);

    // $result = A Response object that wrap: '{"result":{"code":"000.200.100","description":"successfully created checkout"},"buildNumber":"977894f754b0f715838835a65fdbfee69a2e808d@2017-07-07 08:04:06 +0000","timestamp":"2017-07-11 09:22:50+0000","ndc":"E0AF41DAA0A40982EE3AAD5512E9F174.sbg-vm-tx02","id":"E0AF41DAA0A40982EE3AAD5512E9F174.sbg-vm-tx02"}';
