Asynchronous Server-to-Server: Initial Payment
##############################################

The first step is to send a server-to-server initial payment request with the ``paymentBrand`` and ``shopperResultUrl``. The ``shopperResultUrl`` must be url-encoded.

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $paymentBrandRequiredParameters = array(
        'paymentType' => 'PA',
        'amount' => '92.12',
        'currency' => 'EUR',
        'paymentBrand' => 'PAYPAL',
        'shopperResultUrl' => 'https://foo.com/payment/redirect',
    ); // This is just example for PAYPAL, you must follow the Payment Brand's required parameters.

    $result = $opp->asyncServerToServer()->pay($paymentBrandRequiredParameters);

    // $result = A Response object that wrap: '{"id":"8a82944a5d2c3545015d3133138902f7","paymentType":"PA","paymentBrand":"PAYPAL","amount":"92.12","currency":"EUR","descriptor":"6902.2598.4162 Paypal_Channel ","result":{"code":"000.200.000","description":"transaction pending"},"resultDetails":{"ConnectorTxID3":"2c3545015d3133138902f7","ConnectorTxID2":"8a82944a","AcquirerResponse":"Success","ConnectorTxID1":"8a82944a5d2c3545015d3133138902f7","clearingInstituteName":"PayPal Test"},"redirect":{"url":"https://test.ppipe.net/connectors/simulator;jsessionid=3536E151BB214EC4364C871C09F716A4.sbg-vm-con02","parameters":[{"name":"ndcid","value":"8a8294174b7ecb28014b9699220015ca_2777ec10a7264efa90d5e958e6ae014c"},{"name":"useraction","value":"commit"},{"name":"cmd","value":"_express-checkout"},{"name":"token","value":"EC-4MJ162715G749641T"},{"name":"connector","value":"PAYPAL"}]},"buildNumber":"977894f754b0f715838835a65fdbfee69a2e808d@2017-07-07 08:04:06 +0000","timestamp":"2017-07-11 10:30:17+0000","ndc":"8a8294174b7ecb28014b9699220015ca_2777ec10a7264efa90d5e958e6ae014c"}';
