Asynchronous Server-to-Server: Get Payment Status
#################################################

After the payment has been completed/verified, the shopper will be redirected back to ``shopperResultUrl`` with some parameters in query string. Just grab the ``id`` to get the payment status.

Example:

.. code-block:: php

    require_once '/path/to/payreto_opp_lib/src/Opp.php';

    use Payreto\Opp\Opp;

    $authentication = array(
        'authentication.userId' => '8a8294174b7ecb28014b9699220015cc',
        'authentication.password' => 'sy6KJsT8',
        'authentication.entityId' => '8a8294174b7ecb28014b9699220015ca',
    );

    $apiVersion = 'v1';

    $runningInProduction = false; // this should be set to true in production

    $opp = new Opp($authentication, $apiVersion, $runningInProduction);

    $paymentId = '8a82944a4cc25ebf014cc2c782423202';

    $result = $opp->asyncServerToServer()->getPaymentStatus($paymentId);

    // $result = A Response object that wrap: '{"id":"8a82944a4cc25ebf014cc2c782423202","paymentBrand":"AMEX","result":{"code":"000.100.110","description":"Request successfully processed in 'Merchant in Integrator Test Mode'"},"card":{"bin":"******","last4Digits":"7770","holder":"Jane Jones","expiryMonth":"05","expiryYear":"2018"},"buildNumber":"977894f754b0f715838835a65fdbfee69a2e808d@2017-07-07 08:04:06 +0000","timestamp":"2015-04-16 15:09:57+0000","ndc":"8a8294174b7ecb28014b9699220015ca_7ff716842e854bf7bfe2f3bab54753db"}';
